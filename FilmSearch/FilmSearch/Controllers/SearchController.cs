﻿using FilmSearch.Controllers.Models;
using FilmSearch.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FilmSearch.Controllers
{
    public class SearchController : ApiController
    {
        private FilmBLL FilmDB= null;
        
        public SearchController()
        {
            this.FilmDB = new FilmBLL();
        }

        public FilmInfo[] Get(string CinermaName, string FilmName, string s2Time2, string Date, string CinermaCheck, string FilmCheck)
        {
            DateTime date;
            DateTime time;

            bool isSearchName = false;
            bool isSearchCinerma = false;

            if (CinermaCheck != null)
                if (CinermaCheck.ToLower() == "on")
                    isSearchName = true;

            if (FilmCheck != null)
                if (FilmCheck.ToLower() == "on")
                    isSearchCinerma = true;

            if (s2Time2 != null)
            {
                try
                {
                    time = DateTime.Parse(s2Time2);
                }
                catch   (Exception ex)
                {
                   
                }
            }

            if (Date != null)
            {
                try
                {
                    date = DateTime.Parse(Date);
                }
                catch (Exception ex)
                {

                }
            }

            return FilmDB.getFilm(FilmName);
        }
    }
}
